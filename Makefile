
.PHONY: generate
generate:
	java -jar openapi-generator-cli-5.2.0.jar generate -i spec/openapi-v1.yaml -c open-api-conf.yaml -g java -o generated
