/*
 * Ubiquity REST API
 * Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ### Protocols #### Mainnet The following protocols are currently supported: * bitcoin * ethereum * polkadot * xrp * algorand * stellar * dogecoin * oasis * near * litecoin * bitcoincash * tezos  #### Testnet * bitcoin/testnet * ethereum/ropsten * dogecoin/testnet * litecoin/testnet * bitcoincash/testnet  #### Native Ubiquity provides native access to all Blockchain nodes it supports. * bitcoin/(mainnet | testnet) - [RPC Documentation](https://developer.bitcoin.org/reference/rpc/) * ethereum/(mainnet | ropsten) - [RPC Documentation](https://ethereum.org/en/developers/docs/apis/json-rpc/) * polkadot/mainnet - [Sidecar API Documentation](https://paritytech.github.io/substrate-api-sidecar/dist/) * polkadot/mainnet/http-rpc - [Polkadot RPC Documentation](https://polkadot.js.org/docs/substrate/rpc/) * algorand/mainnet - [Algod API Documentation](https://developer.algorand.org/docs/reference/rest-apis/algod/) * stellar/mainnet - [Stellar Horizon API Documentation](https://developers.stellar.org/api) * dogecoin/(mainnet | testnet) - [Dogecoin API Documentaion](https://developer.bitcoin.org/reference/rpc/) * oasis/mainnet - [Oasis Rosetta Gateway Documentation](https://www.rosetta-api.org/docs/api_identifiers.html#network-identifier) * near/mainnet - [NEAR RPC Documentation](https://docs.near.org/docs/api/rpc) * litecoin/mainnet - [Litecoin RPC Documentation](https://litecoin.info/index.php/Litecoin_API) * bitcoincash/mainnet - [Bitcoin Cash RPC Documentation](https://docs.bitcoincashnode.org/doc/json-rpc/) * tezos/mainnet - [Tezos RPC Documentation](https://tezos.gitlab.io/developer/rpc.html)   A full URL example: https://ubiquity.api.blockdaemon.com/v1/bitcoin/mainnet  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available. 
 *
 * The version of the OpenAPI document: 3.0.0
 * Contact: support@blockdaemon.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.gitlab.blockdaemon.ubiquity.model;

import java.util.Objects;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.gitlab.blockdaemon.ubiquity.invoker.JSON;


/**
 * TxConfirmation
 */
@JsonPropertyOrder({
  TxConfirmation.JSON_PROPERTY_CURRENT_HEIGHT,
  TxConfirmation.JSON_PROPERTY_TX_ID,
  TxConfirmation.JSON_PROPERTY_CONFIRMATIONS
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class TxConfirmation {
  public static final String JSON_PROPERTY_CURRENT_HEIGHT = "current_height";
  private Long currentHeight;

  public static final String JSON_PROPERTY_TX_ID = "tx_id";
  private String txId;

  public static final String JSON_PROPERTY_CONFIRMATIONS = "confirmations";
  private Long confirmations;


  public TxConfirmation currentHeight(Long currentHeight) {
    this.currentHeight = currentHeight;
    return this;
  }

   /**
   * Current Block Number
   * @return currentHeight
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "150123", value = "Current Block Number")
  @JsonProperty(JSON_PROPERTY_CURRENT_HEIGHT)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public Long getCurrentHeight() {
    return currentHeight;
  }


  @JsonProperty(JSON_PROPERTY_CURRENT_HEIGHT)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setCurrentHeight(Long currentHeight) {
    this.currentHeight = currentHeight;
  }


  public TxConfirmation txId(String txId) {
    this.txId = txId;
    return this;
  }

   /**
   * Transaction hash
   * @return txId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "0x123456070D674D44a7F9cb4Ab272bd88fAd004b5", value = "Transaction hash")
  @JsonProperty(JSON_PROPERTY_TX_ID)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public String getTxId() {
    return txId;
  }


  @JsonProperty(JSON_PROPERTY_TX_ID)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setTxId(String txId) {
    this.txId = txId;
  }


  public TxConfirmation confirmations(Long confirmations) {
    this.confirmations = confirmations;
    return this;
  }

   /**
   * Total transaction confirmations
   * @return confirmations
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(example = "10", value = "Total transaction confirmations")
  @JsonProperty(JSON_PROPERTY_CONFIRMATIONS)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)

  public Long getConfirmations() {
    return confirmations;
  }


  @JsonProperty(JSON_PROPERTY_CONFIRMATIONS)
  @JsonInclude(value = JsonInclude.Include.USE_DEFAULTS)
  public void setConfirmations(Long confirmations) {
    this.confirmations = confirmations;
  }


  /**
   * Return true if this tx_confirmation object is equal to o.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TxConfirmation txConfirmation = (TxConfirmation) o;
    return Objects.equals(this.currentHeight, txConfirmation.currentHeight) &&
        Objects.equals(this.txId, txConfirmation.txId) &&
        Objects.equals(this.confirmations, txConfirmation.confirmations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currentHeight, txId, confirmations);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TxConfirmation {\n");
    sb.append("    currentHeight: ").append(toIndentedString(currentHeight)).append("\n");
    sb.append("    txId: ").append(toIndentedString(txId)).append("\n");
    sb.append("    confirmations: ").append(toIndentedString(confirmations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

