# Ubiquity Java client

A Java client to the Ubiquity service of blockdaemon.com.
 

# Development
This project uses https://projectlombok.org/setup/gradle and is designed for JDK 8+

# Usage

## Basic

```java
 UbiquityClient client = new UbiquityClientBuilder().authBearerToken("token").build();
 
 Block block = client.block().getBlock(Platforms.BITCOIN, Networks.MAIN_NET, "current");
```

An API URL may also be specified if you have a personal Ubiquity endpoint (Ubiquity's prod endpoint `https://ubiquity.api.blockdaemon.com/v1` is set as the default value [here](https://gitlab.com/Blockdaemon/ubiquity/ubiquity-java-client/-/blob/master/generated/src/main/java/com/gitlab/blockdaemon/ubiquity/invoker/ApiClient.java#L63)): 

```java
 UbiquityClient client = new UbiquityClientBuilder().api("url").authBearerToken("token").build();
 
 Block block = client.block().getBlock(Platforms.BITCOIN, Networks.MAIN_NET, "current");
```


## Paginated APIs

Certain resources contain more data than can be practically returned in a single request. In these resources the data is split across multiple responses where each response returns a subset of the items requested and a continuation token.

Requests for the first page of data should not contain a continuation token. To get the next batch of items, the continuation token should be passed with the subsequent request. If no continuation token is returned, all of the available data has been returned.

Initial request to paged APIs should not include a continuation. If no limit is supplied, the default of 25 will be applied.

```java
TxPage txPage1 = client.transactions().getTxs(Platform.ETHEREUM, Network.MAIN_NET, Order.DESC, "", null, null);
```

To continue through the pages of transactions, the continuation from the previous page must be supplied to the next request

```java
String continuationFromPreviousPage = txPage1.getContinuation();
TxPage txPage2 = client.transactions().getTxs(Platform.ETHEREUM, Network.MAIN_NET, Order.DESC, continuationFromPreviousPage);
```

## Custom networks and platforms

New protocols or networks may not always have enums explicitly defined within the client. However, these protocols and networks can be accessed via the `name()` method:

```java
Block block = client.block().getBlock(Platform.name("algorand"), Network.name("mainnet"), 685700);
```

Further examples are provided in the [example project](examples/src/main/java/com/gitlab/blockdaemon/ubiquity/example/UbiquityClientExample.java).

## Sending transactions

Ubiquity can send signed transactions, which can be created by the client for both Bitcoin and Ethereum. For Bitcoin a signed transaction can be created using the functionality within the client:

```java
    UbiquityClient client = new UbiquityClientBuilder().authBearerToken("UBIQUITY_AUTH_TOKEN").build();

    final String privateKey = "92VNZDvn5NWbtALVxv6s1cKdKVq4Udd6zQ6SxgVK87qE3x7gZEZ";
    final String hashOfPrevTransaction = "245cd89353c21bb3a9afffd6c4c219d458e3b9facde70885f6e2c7856631b09e";
    final String changeAddress = "mmDDkcfXF5co6itzXrivWyxut7XifYywtR";
    final String outputAddress = "mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt";

    PrivateKeyInfo privateKeyInfo = BtcUtils.decodePrivateKey(privateKey);
    KeyPair keyPair = new KeyPair(privateKeyInfo, Address.Type.PUBLIC_KEY_TO_ADDRESS_LEGACY);
    
    // Specify the unspent output of the transactions as well as the value to use as
    // input the the current transaction
    UnspentOutputInfo unspentOutput = new UnspentOutputInfo()
        .keys(keyPair)
        .txHash(hashOfPrevTransaction)
        .value(131000L)
        .outputIndex(0)
        .build();

    // An estimate for the fee can be retrieved from the Ubiquity API
    final FeeEstimate suggestedFee = client.transactions().feeEstimate(Platform.BITCOIN, Network.MAIN_NET);

    // Parse freeform object
    ObjectMapper objectMapper = new ObjectMapper();

    FeeEstimateEstimatedFees feeData = suggestedFee.getEstimatedFees();
    Object fastFeeObj = Objects.requireNonNull(feeData).getFast();
    String fastStr = objectMapper.convertValue(fastFeeObj, String.class);

    // Create a signed transaction
    final SignedTx signedTx = new BtcTransactionBuilder()
        .fee(fastStr)
        .amountToSend(121000L)
        .changeAddress(changeAddress)
        .outputAddress(outputAddress)
        .addUnspentOutput(unspentOutput)
        .build();

    // Send the signed transaction to the Ubiquity API and receive the id of the
    // transaction
    TxReceipt receipt = client.transactions().txSend(Platform.BITCOIN, Network.TEST_NET, signedTx);
```

For Ethereum signed transactions are created using the Web3j library:

```java
    UbiquityClient client = new UbiquityClientBuilder().authBearerToken("UBIQUITY_AUTH_TOKEN").build();

    String fromPrivateKey = "91d75b8d4411a4cce7be104f3b4c149e583f1c2a6fdea78ad3498597ae997ed3";
    Credentials credentials = Credentials.create(fromPrivateKey);

    String toPublicKey = "0x32Ceb3C8409742A446c012Ed94DFa5672c28692B";
    BigInteger value = Convert.toWei("1.0", Convert.Unit.ETHER).toBigInteger();
    BigInteger nonce = BigInteger.valueOf(4);

    // use 1509999997 as gas price
    BigInteger gasprice = new BigInteger("1509999997");
    BigInteger gaslimit = BigInteger.valueOf(21000);

    // To prevent replay attacks the chain id should also be included
    // https://chainid.network/
    int chainId = 3;

    // Signed transactions can be created using the Web3j library
    RawTransaction rawTransaction = RawTransaction.createEtherTransaction(nonce, gasprice, gaslimit, toPublicKey, value);

    // A signed ubiquity tx can the be created through the EthTransactionBuilder
    // class
    SignedTx tx = new EthTransactionBuilder()
        .chainId(chainId)
        .credentials(credentials)
        .rawTransaction(rawTransaction)
        .build();

    // The signed transaction can then be sent using the Ubiquity API
    TxReceipt receipt = client.transactions().txSend(Platform.ETHEREUM, Network.name("ropsten"), tx);
    System.out.println(receipt.getId());
```

Further examples are provided in the example projects: [Bitcoin](examples/src/main/java/com/gitlab/blockdaemon/ubiquity/example/BtcTransactionExample.java), [Ethereum](examples/src/main/java/com/gitlab/blockdaemon/ubiquity/example/EthTransactionExample.java).

## Websockets support

Create a new websocket client using `try` with resources, so the connection will be gracefully closed after use. The client implements `AutoCloseable` so that it may be used with `try` with resources. Closing the socket gracefully may take a few seconds.

There are currently three channels: Tx and BlockIdentifiers. The focus of the notifications may additionally be refined using the `details` section.

For example, the following code restricts the Tx channel notifications to only those about address "0x123":
 
```java
	private static BiConsumer<WebsocketClient, TxNotification> txConsumer = new BiConsumer<>() {
		@Override
		public void accept(WebsocketClient t, TxNotification u) {
			System.out.println(u);
		}
	};

	try (WebsocketClient wsClient = client.ws(Platform.ETHEREUM, Network.MAIN_NET);) {
		final Subscription<TxNotification> subscription = Subscription.tx(singletonMap("addresses", singletonList("0x123")), txConsumer);
		wsClient.subscribe(subscription);
	}	
```

If the connection to the server is lost, the connection will be retried every 10 seconds by default. This can be configured on the client using the `reconnectDelay()` method.

When the websocket is closed gracefully, already-enqueued messages will continue to be transmitted. However, new messages will be refused.

Further examples are provided in the [example project](examples/src/main/java/com/gitlab/blockdaemon/ubiquity/example/WebsocketExample.java).
 

# Dependency

Add a `repositories` section to your build.gradle file:
	
```
repositories {
    maven {
        url "https://gitlab.com/api/v4/projects/26939001/packages/maven"
        name "ubiquity-client-gitLab"
    }
}
```
	
```
dependencies {
		implementation group: "com.gitlab.blockdaemon", name: "ubiquity-client", version: "1.0.0"
		implementation "org.web3j:core:4.8.4"
}
```
	
Further explanations of these instructions are given [here](https://docs.gitlab.com/ee/user/packages/maven_repository/index.html).

 
