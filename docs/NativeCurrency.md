

# NativeCurrency


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetPath** | **String** | Asset path of transferred currency | 
**symbol** | **String** | Currency symbol |  [optional]
**name** | **String** | Name of currency |  [optional]
**decimals** | **Integer** | Decimal places right to the comma |  [optional]
**type** | **String** |  | 



