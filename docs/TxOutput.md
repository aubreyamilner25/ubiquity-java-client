

# TxOutput


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**StatusEnum**](#StatusEnum) | Result status of the transaction output. |  [optional]
**isSpent** | **Boolean** | If the transaction output was spent or not, if the value is true the &#x60;spent&#x60; transaction object will be presented |  [optional]
**value** | **Long** | Amount of transaction output |  [optional]
**mined** | [**TxMinify**](TxMinify.md) |  |  [optional]
**spent** | [**TxMinify**](TxMinify.md) |  |  [optional]



## Enum: StatusEnum

Name | Value
---- | -----
MINED | &quot;mined&quot;
UNKNOWN | &quot;unknown&quot;



