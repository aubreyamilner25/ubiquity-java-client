# TransactionsApi

All URIs are relative to *https://ubiquity.api.blockdaemon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**estimateFee**](TransactionsApi.md#estimateFee) | **GET** /v2/{platform}/{network}/tx/estimate_fee | Get fee estimate
[**feeEstimate**](TransactionsApi.md#feeEstimate) | **GET** /v1/{platform}/{network}/tx/estimate_fee | Get fee estimate
[**getTx**](TransactionsApi.md#getTx) | **GET** /v2/{platform}/{network}/tx/{id} | Transaction By Hash
[**getTxByHashAndIndex**](TransactionsApi.md#getTxByHashAndIndex) | **GET** /v1/{platform}/{network}/tx/{id}/{index} | Transaction output by hash and index
[**getTxConfirmations**](TransactionsApi.md#getTxConfirmations) | **GET** /v1/{platform}/{network}/tx/{id}/confirmations | Transaction confirmations By Hash
[**getTxs**](TransactionsApi.md#getTxs) | **GET** /v2/{platform}/{network}/txs | All Transactions
[**txSend**](TransactionsApi.md#txSend) | **POST** /v2/{platform}/{network}/tx/send | Submit a signed transaction



## estimateFee

> String estimateFee(platform, network, confirmedWithinBlocks)

Get fee estimate

Get a fee estimation in decimals from the network.
If supported by the platform, the number of blocks used to make
the estimation can be customized by the confirmed_within_blocks query parameter.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.TransactionsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        TransactionsApi apiInstance = new TransactionsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        Integer confirmedWithinBlocks = 10; // Integer | The number of blocks you would like the transaction to be processed within. Lower numbers produce higher fees. 
        try {
            String result = apiInstance.estimateFee(platform, network, confirmedWithinBlocks);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling TransactionsApi#estimateFee");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **confirmedWithinBlocks** | **Integer**| The number of blocks you would like the transaction to be processed within. Lower numbers produce higher fees.  | [optional] [default to 10]

### Return type

**String**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain, application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | A fee estimation from bitcoin (satoshis) |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |
| **400** | Bad Request |  -  |


## feeEstimate

> FeeEstimate feeEstimate(platform, network)

Get fee estimate

Get a fee estimation in decimals from the ubiquity fee estimation service.
Currently supported for Bitcoin and Ethereum. Endpoint will return 3 fee estimations
fast, medium and slow


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.TransactionsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        TransactionsApi apiInstance = new TransactionsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        try {
            FeeEstimate result = apiInstance.feeEstimate(platform, network);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling TransactionsApi#feeEstimate");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |

### Return type

[**FeeEstimate**](FeeEstimate.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Platforms overview |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## getTx

> Tx getTx(platform, network, id)

Transaction By Hash

### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.TransactionsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        TransactionsApi apiInstance = new TransactionsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String id = "0xF00Fa860473130C1df10707223E66Cb4B839B165"; // String | Transaction ID/Hash
        try {
            Tx result = apiInstance.getTx(platform, network, id);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling TransactionsApi#getTx");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **id** | **String**| Transaction ID/Hash |

### Return type

[**Tx**](Tx.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Transaction |  -  |
| **400** | Bad Request |  -  |
| **401** | Invalid or expired token |  -  |
| **404** | Not Found |  -  |
| **429** | Rate limit exceeded |  -  |


## getTxByHashAndIndex

> TxOutput getTxByHashAndIndex(platform, network, id, index)

Transaction output by hash and index

### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.TransactionsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        TransactionsApi apiInstance = new TransactionsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String id = "0xF00Fa860473130C1df10707223E66Cb4B839B165"; // String | Transaction ID/Hash
        Integer index = 0; // Integer | Transaction output index
        try {
            TxOutput result = apiInstance.getTxByHashAndIndex(platform, network, id, index);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling TransactionsApi#getTxByHashAndIndex");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **id** | **String**| Transaction ID/Hash |
 **index** | **Integer**| Transaction output index |

### Return type

[**TxOutput**](TxOutput.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Transaction output |  -  |
| **400** | Bad Request |  -  |
| **401** | Invalid or expired token |  -  |
| **404** | Not Found |  -  |
| **429** | Rate limit exceeded |  -  |


## getTxConfirmations

> TxConfirmation getTxConfirmations(platform, network, id)

Transaction confirmations By Hash

### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.TransactionsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        TransactionsApi apiInstance = new TransactionsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String id = "0xF00Fa860473130C1df10707223E66Cb4B839B165"; // String | Transaction ID/Hash
        try {
            TxConfirmation result = apiInstance.getTxConfirmations(platform, network, id);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling TransactionsApi#getTxConfirmations");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **id** | **String**| Transaction ID/Hash |

### Return type

[**TxConfirmation**](TxConfirmation.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Transaction confirmations |  -  |
| **400** | Bad Request |  -  |
| **401** | Invalid or expired token |  -  |
| **404** | Not Found |  -  |
| **429** | Rate limit exceeded |  -  |


## getTxs

> TxPage getTxs(platform, network, order, continuation, limit, assets)

All Transactions

Get all transactions on the platform, starting with the lastest one. Each call returns a slice of the entire list. Use the returned continuation token to get the next part.

### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.TransactionsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        TransactionsApi apiInstance = new TransactionsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String order = "order_example"; // String | Pagination order
        String continuation = "8185.123"; // String | Continuation token from earlier response
        Integer limit = 25; // Integer | Max number of items to return in a response. Defaults to 25 and is capped at 100. 
        String assets = "ethereum/native/eth"; // String | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect.
        try {
            TxPage result = apiInstance.getTxs(platform, network, order, continuation, limit, assets);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling TransactionsApi#getTxs");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **order** | **String**| Pagination order | [optional] [enum: desc, asc]
 **continuation** | **String**| Continuation token from earlier response | [optional]
 **limit** | **Integer**| Max number of items to return in a response. Defaults to 25 and is capped at 100.  | [optional]
 **assets** | **String**| Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | [optional]

### Return type

[**TxPage**](TxPage.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Transactions |  -  |
| **401** | Invalid or expired token |  -  |
| **403** | Invalid continuation |  -  |
| **429** | Rate limit exceeded |  -  |


## txSend

> TxReceipt txSend(platform, network, signedTx)

Submit a signed transaction

Submit a signed transaction to the network.

**Note**: A successful transaction may still be rejected on chain or not processed due to a too low fee.
You can monitor successful transactions through Ubiquity websockets.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.TransactionsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        TransactionsApi apiInstance = new TransactionsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        SignedTx signedTx = new SignedTx(); // SignedTx | 
        try {
            TxReceipt result = apiInstance.txSend(platform, network, signedTx);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling TransactionsApi#txSend");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **signedTx** | [**SignedTx**](SignedTx.md)|  |

### Return type

[**TxReceipt**](TxReceipt.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | A submitted Transaction ID |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |
| **400** | Bad Request |  -  |

