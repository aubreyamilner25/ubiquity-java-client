

# PlatformsOverview


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**platforms** | [**List&lt;PlatformsOverviewPlatforms&gt;**](PlatformsOverviewPlatforms.md) | List of items each describing a pair of supported platform and network. |  [optional]



