

# Block


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Long** | Block number |  [optional]
**id** | **String** | Block hash |  [optional]
**parentId** | **String** | Parent block hash |  [optional]
**date** | **Long** | Unix timestamp |  [optional]
**txIds** | **List&lt;String&gt;** | Complete list of transaction IDs |  [optional]
**txs** | [**List&lt;Tx&gt;**](Tx.md) | Partial list of normalized transactions (not filtered or unknown model) |  [optional]
**supply** | [**Map&lt;String, Supply&gt;**](Supply.md) | Coin supplies with asset paths as keys |  [optional]



