

# ListEventResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;NFTEvent&gt;**](NFTEvent.md) |  |  [optional]
**meta** | [**Meta**](Meta.md) |  |  [optional]



