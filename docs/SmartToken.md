

# SmartToken


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | Name of token mechanism (smart contract) | 
**contract** | **String** | Address of contract | 



