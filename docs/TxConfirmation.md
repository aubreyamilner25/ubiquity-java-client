

# TxConfirmation


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currentHeight** | **Long** | Current Block Number |  [optional]
**txId** | **String** | Transaction hash |  [optional]
**confirmations** | **Long** | Total transaction confirmations |  [optional]



