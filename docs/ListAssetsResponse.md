

# ListAssetsResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;Asset&gt;**](Asset.md) |  |  [optional]
**meta** | [**Meta**](Meta.md) |  |  [optional]



