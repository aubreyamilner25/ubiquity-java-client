

# ReportField


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**protocol** | **String** | The protocol the address relates to | 
**address** | **String** | The wallet/account the transaction occurred | 
**currency** | **String** | The currency symbol | 
**eventId** | **String** | The ID of the event within a transaction | 
**block** | **Long** | The block number the transaction occurred on | 
**timestamp** | **Integer** | The unix timestamp when the transaction was added to a block | 
**hash** | **String** | The transaction ID | 
**action** | **String** | The action type e.g. Transfer, Deposit, Staking Reward etc.. | 
**value** | **String** | The amount of currency involved in the transaction (smallest unit) | 
**senderAddress** | **String** | The address where the funds originated | 
**fee** | **String** | How much was charged as a fee for processing the transaction | 
**decimals** | **Integer** | The number of decimals in one coin, used to convert smallest unit to 1 whole coin if needed | 
**meta** | [**ReportFieldMeta**](ReportFieldMeta.md) |  |  [optional]



