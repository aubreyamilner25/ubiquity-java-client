

# Collection


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**imageUrl** | **String** |  |  [optional]
**contracts** | [**List&lt;Contract&gt;**](Contract.md) |  |  [optional]
**meta** | **Object** |  |  [optional]



