

# FeeEstimate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mostRecentBlock** | **Integer** | Most recent block |  [optional]
**estimatedFees** | [**FeeEstimateEstimatedFees**](FeeEstimateEstimatedFees.md) |  |  [optional]



