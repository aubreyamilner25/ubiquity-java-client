package com.gitlab.blockdaemon.ubiquity.ws;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import com.gitlab.blockdaemon.ubiquity.ws.Notification.BlockIdentifierNotification;
import com.gitlab.blockdaemon.ubiquity.ws.Notification.TxNotification;
import com.gitlab.blockdaemon.ubiquity.ws.WebsocketRequest.Params;

import lombok.Data;

@Data
public class Subscription<T extends Notification> {

	private static final String CHANNEL_BLOCK_IDENTIFIERS = "ubiquity.block_identifiers";
	private static final String CHANNEL_TXS = "ubiquity.txs";
	private static final String SUB_ID = "subID";

	private final BiConsumer<WebsocketClient, T> handler;
	private final Map<String, Object> details;
	private final Class<T> typeClazz;
	private final String channel;
	private Integer subId;

	private Subscription(String channel, Class<T> typeClazz, Map<String, Object> details,
			BiConsumer<WebsocketClient, T> handler) {
		this.channel = channel;
		this.typeClazz = typeClazz;
		this.details = details;
		this.handler = handler;
	}

	public static Subscription<BlockIdentifierNotification> blockIdentifiers(BiConsumer<WebsocketClient, BlockIdentifierNotification> handler) {
		return new Subscription<BlockIdentifierNotification>(CHANNEL_BLOCK_IDENTIFIERS,
				BlockIdentifierNotification.class, new HashMap<String, Object>(), handler);
	}

	public static Subscription<BlockIdentifierNotification> blockIdentifiers(Map<String, Object> details, BiConsumer<WebsocketClient, BlockIdentifierNotification> handler) {
		return new Subscription<BlockIdentifierNotification>(CHANNEL_BLOCK_IDENTIFIERS,
				BlockIdentifierNotification.class, details, handler);
	}

	public static Subscription<TxNotification> tx(BiConsumer<WebsocketClient, TxNotification> handler) {
		return new Subscription<TxNotification>(CHANNEL_TXS, TxNotification.class, new HashMap<String, Object>(),
				handler);
	}

	public static Subscription<TxNotification> tx(Map<String, Object> details, BiConsumer<WebsocketClient, TxNotification> handler) {
		return new Subscription<TxNotification>(CHANNEL_TXS, TxNotification.class, details, handler);
	}

	public WebsocketRequest getSubscribeRequest() {
		final Params params = new Params();
		params.setChannel(getChannel());
		params.setSubID(getSubId());
		params.setDetail(new HashMap<>(getDetails()));

		final WebsocketRequest websocketRequest = new WebsocketRequest();
		websocketRequest.setMethod(WebsocketRequest.SUBSCRIBE);
		websocketRequest.setParams(params);

		return websocketRequest;
	}

	public WebsocketRequest getUnsubscribeRequest() {
		final Params params = new Params();
		params.setChannel(getChannel());
		params.setSubID(getSubId());
		params.setDetail(new HashMap<>(getDetails()));

		final WebsocketRequest websocketRequest = new WebsocketRequest();
		websocketRequest.setMethod(WebsocketRequest.UNSUBSCRIBE);
		websocketRequest.setParams(params);

		return websocketRequest;
	}

}
