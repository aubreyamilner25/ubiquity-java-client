package com.gitlab.blockdaemon.ubiquity.ws;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gitlab.blockdaemon.ubiquity.ws.Notification.BlockIdentifierNotification;
import com.gitlab.blockdaemon.ubiquity.ws.Notification.TxNotification;

/*
 * Class used to dynamically parse different subid values to different classes
 */
public class NotificationDeserializer extends StdDeserializer<Notification> {

	private static final long serialVersionUID = -2768020880173068696L;
	private final Map<String, Class<? extends Notification>> registry = new HashMap<>();

	public NotificationDeserializer() {
		super(Notification.class);
		this.registry.put("ubiquity.block_identifiers", BlockIdentifierNotification.class);
		this.registry.put("ubiquity.txs", TxNotification.class);
	}

	@Override
	public boolean isCachable() { return false; }

	@Override
	public Notification deserialize(JsonParser jp, DeserializationContext ctxt)	throws IOException, JsonProcessingException {
		final ObjectMapper mapper = (ObjectMapper) jp.getCodec();

		final TreeNode rootTree = mapper.readTree(jp);

		// rootTree may be NullNode if no value is present
		if(rootTree instanceof ObjectNode) {
			final ObjectNode root = (ObjectNode) rootTree;

			final JsonNode channelJson = root.get("channel");

			if (channelJson == null) {
				return null;
			}

			final String channel = channelJson.asText();
			if (this.registry.containsKey(channel)) {
				return rootTree.traverse(jp.getCodec()).readValueAs(this.registry.get(channel));
			}
		}

		return null;
	}

}