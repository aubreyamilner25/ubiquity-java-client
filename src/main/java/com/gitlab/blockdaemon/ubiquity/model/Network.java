package com.gitlab.blockdaemon.ubiquity.model;

public enum Network {

	MAIN_NET("mainnet"), TEST_NET("testnet"), OTHER_NET();

	private String network;

	Network(String network) {
		this.network = network;
	}

	Network() {
	}

	public String getNetwork() {
		return network;
	}

	public static Network name(String networkName) {
		Network network = Network.OTHER_NET;
		network.network = networkName;
		return network;
	}

}
