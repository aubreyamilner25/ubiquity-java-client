package com.gitlab.blockdaemon.ubiquity.tx.btc;

import lombok.Data;

@Data
public class KeyPair {
	public final byte[] publicKey;
	public final Address address;
	public final PrivateKeyInfo privateKey;


	public KeyPair(String address, byte[] publicKey, PrivateKeyInfo privateKey) {
		this.publicKey = publicKey;
		this.address = Address.decode(address);
		this.privateKey = privateKey;
	}


	public KeyPair( PrivateKeyInfo privateKeyInfo, Address.Type publicKeyRepresentation) {
		if (privateKeyInfo.getPrivateKeyDecoded() == null) {
			this.publicKey = null;
			this.address = null;
		} else {
			this.publicKey = BtcService.generatePublicKey(privateKeyInfo.getPrivateKeyDecoded(), privateKeyInfo.isPublicKeyCompressed());
			String addressStr;
			switch (publicKeyRepresentation) {
			case PUBLIC_KEY_TO_ADDRESS_LEGACY:
				addressStr = Address.publicKeyToAddress(privateKeyInfo.isTestNet(), this.publicKey);
				break;
			case PUBLIC_KEY_TO_ADDRESS_P2WKH:
				addressStr = Address.publicKeyToP2wkhAddress(privateKeyInfo.isTestNet(), this.publicKey);
				break;
			case PUBLIC_KEY_TO_ADDRESS_P2SH_P2WKH:
				addressStr = Address.publicKeyToP2shP2wkhAddress(privateKeyInfo.isTestNet(), this.publicKey);
				break;
			default:
				throw new RuntimeException("Unknown publicKeyRepresentation " + publicKeyRepresentation);
			}
			this.address = Address.decode(addressStr);
		}
		this.privateKey = privateKeyInfo;
	}
}
