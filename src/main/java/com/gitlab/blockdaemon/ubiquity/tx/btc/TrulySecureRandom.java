package com.gitlab.blockdaemon.ubiquity.tx.btc;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.prng.DigestRandomGenerator;
import org.bouncycastle.crypto.prng.ThreadedSeedGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrulySecureRandom extends java.security.SecureRandom {

	private static final long serialVersionUID = -7802874633037581684L;
	private static final String TAG = "SecureRandom";
	private final DigestRandomGenerator generator;
	private boolean initialized;
	private static final Logger LOGGER = LoggerFactory.getLogger(TrulySecureRandom.class);


	TrulySecureRandom() {
		this.generator = new DigestRandomGenerator(new SHA256Digest());
	}

	void addSeedMaterial(long seed) {
		this.generator.addSeedMaterial(seed);
	}

	private void addSeedMaterial(byte[] seed) {
		this.generator.addSeedMaterial(seed);
	}

	@Override
	public int nextInt() {
		final byte[] buf = new byte[4];
		nextBytes(buf);
		return ((buf[0] & 0xff) << 24) | ((buf[1] & 0xff) << 16) | ((buf[2] & 0xff) << 8) | (buf[3] & 0xff);
	}

	@Override
	public int nextInt(int n) {
		final int anInt = nextInt();
		return Math.abs(anInt == Integer.MIN_VALUE ? Integer.MAX_VALUE : anInt) % n;
	}

	@Override
	public synchronized void nextBytes(byte[] bytes) {
		if (!this.initialized) {
			final long start = System.currentTimeMillis();
			final ThreadedSeedGenerator threadedSeedGenerator = new ThreadedSeedGenerator();
			do {
				addSeedMaterial(threadedSeedGenerator.generateSeed(64, true));
				try {
					Thread.sleep(1);
				} catch (final InterruptedException ignored) {
				}
				addSeedMaterial(threadedSeedGenerator.generateSeed(32, false));
			} while (Math.abs(System.currentTimeMillis() - start) < 1000);

			addSeedMaterial(System.nanoTime());
			addSeedMaterial(System.currentTimeMillis());
			addSeedMaterial(new java.security.SecureRandom().generateSeed(128));

			final ExecutorService executor = Executors.newSingleThreadExecutor();
			try {
				final Future<?> future = executor.submit(() -> {
					final byte[] devRandomSeed = getDevRandomSeed();
					if (devRandomSeed != null) {
						addSeedMaterial(devRandomSeed);
					}
				});
				future.get(3, TimeUnit.SECONDS);
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				LOGGER.error(TAG, "/dev/random read error", e);
			} finally {
				executor.shutdownNow();
			}
			this.initialized = true;
		}
		this.generator.nextBytes(bytes);
	}


	@Override
	public String getAlgorithm() {
		return "BouncyCastle";
	}

	private byte[] getDevRandomSeed() {
		byte[] buf = null;
		final File file = new File("/dev/random");
		try (FileInputStream inputStream = new FileInputStream(file)) {
			buf = new byte[16];
			for (int i = 0; i < buf.length; i++) {
				final int ch = inputStream.read();
				if (ch == -1) {
					return null;
				}
				buf[i] = (byte) ch;
			}
		} catch (final Exception ignored) {
		}
		return buf;
	}

	@Override
	public void setSeed(byte[] seed) {
		LOGGER.info("SecureRandom", "setting seed " + BtcService.toHex(seed) + " was ignored");
	}

	@Override
	public void setSeed(long seed) {
		LOGGER.info("SecureRandom", "setting seed " + seed + " was ignored");
	}

	@Override
	public byte[] generateSeed(int numBytes) {
		throw new RuntimeException("not supported");
	}

	@Override
	public boolean nextBoolean() {
		throw new RuntimeException("not supported");
	}

	@Override
	public double nextDouble() {
		throw new RuntimeException("not supported");
	}

	@Override
	public float nextFloat() {
		throw new RuntimeException("not supported");
	}

	@Override
	public synchronized double nextGaussian() {
		throw new RuntimeException("not supported");
	}

	@Override
	public long nextLong() {
		throw new RuntimeException("not supported");
	}

}
