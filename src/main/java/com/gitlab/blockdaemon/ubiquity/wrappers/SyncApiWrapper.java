/*
 * Ubiquity REST API
 * Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available. 
 *
 * The version of the OpenAPI document: 2.0.0
 * Contact: support@blockdaemon.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

package com.gitlab.blockdaemon.ubiquity.wrappers;

import com.gitlab.blockdaemon.ubiquity.api.SyncApi;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiResponse;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;

public class SyncApiWrapper {
	private SyncApi api;

	public SyncApiWrapper(SyncApi api) {
		this.api = api;
	}

	public SyncApi raw() {
		return api;
	}

	/**
	 * Get current block ID
	 * 
	 * @param platform Coin platform handle (required)
	 * @param network  Which network to target. Available networks can be found with
	 *                 /{platform} (required)
	 * @return String
	 * @throws ApiException If fail to call the API, e.g. server error or cannot
	 *                      deserialize the response body
	 * @http.response.details
	 *                        <table summary="Response Details" border="1">
	 */
	public String currentBlockID(Platform platform, Network network) throws ApiException {
		ApiResponse<String> localVarResp = this.api.currentBlockIDWithHttpInfo(platform.getPlatform(),
				network.getNetwork());
		return localVarResp.getData();
	}

	/**
	 * Get current block number
	 * 
	 * @param platform Coin platform handle (required)
	 * @param network  Which network to target. Available networks can be found with
	 *                 /{platform} (required)
	 * @return Integer
	 * @throws ApiException If fail to call the API, e.g. server error or cannot
	 *                      deserialize the response body
	 * 
	 */
	public Long currentBlockNumber(Platform platform, Network network) throws ApiException {
		ApiResponse<Long> localVarResp = this.api.currentBlockNumberWithHttpInfo(platform.getPlatform(),
				network.getNetwork());
		return localVarResp.getData();
	}

}
