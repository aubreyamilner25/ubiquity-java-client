/*
 * Ubiquity REST API
 * Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available. 
 *
 * The version of the OpenAPI document: 2.0.0
 * Contact: support@blockdaemon.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

package com.gitlab.blockdaemon.ubiquity.wrappers;

import com.gitlab.blockdaemon.ubiquity.api.PlatformsApi;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiResponse;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;
import com.gitlab.blockdaemon.ubiquity.model.PlatformDetail;
import com.gitlab.blockdaemon.ubiquity.model.PlatformsOverview;

public class PlatformsApiWrapper {
	private PlatformsApi api;

	public PlatformsApiWrapper(PlatformsApi api) {
		this.api = api;
	}

	public PlatformsApi raw() {
		return api;
	}

	/**
	 * Platform Info Provides information about supported endpoints and generic
	 * platform information.
	 * 
	 * @param platform Coin platform handle (required)
	 * @param network  Which network to target. Available networks can be found with
	 *                 /{platform} (required)
	 * @return PlatformDetail
	 * @throws ApiException If fail to call the API, e.g. server error or cannot
	 *                      deserialize the response body
	 */
	public PlatformDetail getPlatform(Platform platform, Network network) throws ApiException {
		ApiResponse<PlatformDetail> localVarResp = this.api.getPlatformEndpointsWithHttpInfo(platform.getPlatform(),
				network.getNetwork());
		return localVarResp.getData();
	}

	/**
	 * Platforms Overview provides a list of supported
	 * platforms and networks.
	 * 
	 * @return PlatformsOverview
	 * @throws ApiException If fail to call the API, e.g. server error or cannot
	 *                      deserialize the response body
	 */
	public PlatformsOverview getPlatforms() throws ApiException {
		ApiResponse<PlatformsOverview> localVarResp = this.api.getPlatformsListWithHttpInfo();
		return localVarResp.getData();
	}
}
