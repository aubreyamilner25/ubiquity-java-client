package com.gitlab.blockdaemon.ubiquity.example;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;

import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;

import com.gitlab.blockdaemon.ubiquity.UbiquityClient;
import com.gitlab.blockdaemon.ubiquity.UbiquityClientBuilder;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;
import com.gitlab.blockdaemon.ubiquity.ws.Notification.BlockIdentifierNotification;
import com.gitlab.blockdaemon.ubiquity.ws.Notification.TxNotification;
import com.gitlab.blockdaemon.ubiquity.ws.Subscription;
import com.gitlab.blockdaemon.ubiquity.ws.WebsocketClient;

public class WebsocketExample {

	// Consumer used to handle the message recieves
	private static BiConsumer<WebsocketClient, TxNotification> txConsumer = new BiConsumer<>() {

		@Override
		public void accept(WebsocketClient client, TxNotification notification) {
			System.out.println("Got a TX notification:: "+ notification.getContent().getId());
		}
	};

	private static BiConsumer<WebsocketClient, BlockIdentifierNotification> blockIdentifierConsumer = (WebsocketClient client, BlockIdentifierNotification notification) -> {
		System.out.println("Got a block identifier notification:: " + notification.getContent().getId());
	};

	public static void main(String... args) throws InterruptedException, ExecutionException {
		final String authBearerToken = System.getenv("AUTH_TOKEN");
		final UbiquityClient client = new UbiquityClientBuilder()
				.authBearerToken(authBearerToken)
				.build();

		// Create a new websocket client using try with resources so the connection will be gracefully closed after use.
		try (WebsocketClient wsClient = client.ws(Platform.ETHEREUM, Network.MAIN_NET);) {

			// Create a subscription to a particular channel.
			final Subscription<TxNotification> txSubscription = Subscription.tx(singletonMap("addresses", singletonList("0x78c115F1c8B7D0804FbDF3CF7995B030c512ee78")), txConsumer);
			wsClient.subscribe(txSubscription).get();

			final Subscription<BlockIdentifierNotification> blockIdentifierSubscription = Subscription.blockIdentifiers(blockIdentifierConsumer);
			wsClient.subscribe(blockIdentifierSubscription).get();

			// Wait for the server to send some new tx to the specified consumer.
			Thread.sleep(40000L);

			// Unsubscribe from the channel
			wsClient.unsubscribe(txSubscription).get();
		}
		// closing the socket gracefully may take a few seconds
		// during the graceful shutdown active subscription handlers may receive notification

	}
}
