package com.gitlab.blockdaemon.ubiquity.example;

import java.math.BigInteger;

import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.utils.Convert;


import com.gitlab.blockdaemon.ubiquity.UbiquityClient;
import com.gitlab.blockdaemon.ubiquity.UbiquityClientBuilder;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;
import com.gitlab.blockdaemon.ubiquity.model.SignedTx;
import com.gitlab.blockdaemon.ubiquity.model.TxReceipt;
import com.gitlab.blockdaemon.ubiquity.tx.eth.EthTransactionBuilder;

public class EthTransactionExample {

	public static void main(String... args) {
		final String authBearerToken = System.getenv("AUTH_TOKEN");
		final UbiquityClient client = new UbiquityClientBuilder().authBearerToken(authBearerToken).build();

		final String fromPrivateKey = "91d75b8d4411a4cce7be104f3b4c149e583f1c2a6fdea78ad3498597ae997ed3";
		final Credentials credentials = Credentials.create(fromPrivateKey);

		final String toPublicKey = "0x32Ceb3C8409742A446c012Ed94DFa5672c28692B";
		final BigInteger value = Convert.toWei("1.0", Convert.Unit.ETHER).toBigInteger();
		final BigInteger nonce = BigInteger.valueOf(4);

		// use 1509999997 as gas price
		BigInteger gasprice = new BigInteger("1509999997");
		BigInteger gaslimit = BigInteger.valueOf(21000);


		// To prevent replay attacks the chain id should also be included
		// https://chainid.network/
		final int chainId = 3;

		// Signed transactions can be created using the web3j library
		final RawTransaction rawTransaction = RawTransaction.createEtherTransaction(nonce, gasprice, gaslimit, toPublicKey, value);

		// A signed ubiquity tx can the be created through the EthTransactionBuilder
		// class
		final SignedTx tx = new EthTransactionBuilder()
				.chainId(chainId)
				.credentials(credentials)
				.rawTransaction(rawTransaction)
				.build();

		// The signed transaction can then be sent using the ubiquity API
		final TxReceipt receipt = client.transactions().txSend(Platform.ETHEREUM, Network.name("ropsten"), tx);
		System.out.println(receipt.getId());
	}

}
